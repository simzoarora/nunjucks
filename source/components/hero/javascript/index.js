/**
 *  @shelf-version: 1.1.0
 */


import '@utilities/in-view'
import '@components/image'
import VideoLoader from '@components/video/loader'
import '@utilities/focus-trap'
import moduleInit from '@utilities/module-init'
import Modal from '@components/modal' // Sync


moduleInit.async('[js-hook-modal]', () =>
    import('@components/modal'),
)

VideoLoader.then(([Platforms, Video]) => {
    Video.default.registerPlatforms({
        native: Platforms.Native,
        youtube: Platforms.Youtube,
        vimeo: Platforms.Vimeo,
    })
}).catch(() => { })
Events.$trigger('modal::bind', { data: { hook: '#modal-example' } })
Events.$trigger('modal::bind', { data: { hook: '#modal-custom' } })

export default Modal